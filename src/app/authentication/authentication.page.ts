import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { AlertController, LoadingController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-authentication',
  templateUrl: './authentication.page.html',
  styleUrls: ['./authentication.page.scss'],
})
export class AuthenticationPage implements OnInit {
  screenHigth: number;
  sizeImg: string;
  formLogin: FormGroup;
  constructor(public alertController: AlertController,
    public loadingController: LoadingController,
    private formBuilder: FormBuilder, private router: Router) {
  }

  ngOnInit() {
    this.initForm();
    this.screenHigth = screen.height * .25;
    this.sizeImg = this.screenHigth + "px";
  }
  initForm() {
    this.formLogin = new FormGroup({
      email: new FormControl(''),
      password: new FormControl('')
    });
  }
  async login() {
    // const loading = await this.loadingController.create({
    //   mode: 'ios'
    // });
    // await loading.present();
    console.log('save data...', this.formLogin.value);
    this.presentAlert();
    this.router.navigate(['/']);
  }
  async presentAlert() {
    const alert = await this.alertController.create({
      header: 'Alert',
      subHeader: 'Subtitle',
      message: 'This is an alert message.',
      buttons: ['OK'],
      mode: 'ios'
    });
    await alert.present();
  }

}
