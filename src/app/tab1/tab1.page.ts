import { Component } from '@angular/core';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { AlertController, LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {
  register: FormGroup;
  constructor(public alertController: AlertController, public loadingController: LoadingController,
    private formBuilder: FormBuilder) {
    this.register = new FormGroup({
      oneData: new FormControl(''),
      twoData: new FormControl(''),
    });
  }
  formInit() {
    this.register = new FormGroup({
      oneData: new FormControl(''),
      twoData: new FormControl(''),
    });
  }
  async saveData() {
    // const loading = await this.loadingController.create({
    //   mode: 'ios'
    // });
    // await loading.present();
    console.log('save data...', this.register.value);
    this.presentAlert();
  }
  async presentAlert() {
    const alert = await this.alertController.create({
      header: 'Alert',
      subHeader: 'Subtitle',
      message: 'This is an alert message.',
      buttons: ['OK'],
      mode: 'ios'
    });
    await alert.present();
  }
}
